FROM python:3.9
WORKDIR /app
COPY . .
RUN pip install pipenv
RUN pipenv install --ignore-pipfile
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
CMD ["./start.sh"]