#!/bin/bash

# Perform migrations
pipenv run python manage.py migrate

# Run server
pipenv run python manage.py runserver 0.0.0.0:8000