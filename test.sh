#!/bin/bash

docker-compose -f docker-compose.dev.yaml exec animaltrade /bin/bash -c "pipenv run python3 manage.py test"