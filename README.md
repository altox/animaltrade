# Animal Trade API Server

## Running

```bash
docker-compose -f docker-compose.dev.yaml up
```

## Testing 

Container should be up 
```bash
./test.sh
```