from django.contrib import admin
from django.urls import path, include

from animaltrade.api import views
from animaltrade.api.modules.animals.urls import get_animals_urls
from animaltrade.api.modules.auth.urls import get_auth_urls
from animaltrade.api.modules.billing.urls import get_billing_urls
from animaltrade.api.modules.lots.urls import get_lots_urls
from animaltrade.api.modules.swagger.urls import get_swagger_urls
from animaltrade.api.modules.user.urls import get_user_urls


def build_server_urls():
    urlpatterns = [
        path('admin/', admin.site.urls),
        path('ping/', views.ping_view),
    ]

    # Auth
    urlpatterns += get_auth_urls()

    # Users
    urlpatterns += get_user_urls()

    # Animals
    urlpatterns += get_animals_urls()

    # Lots
    urlpatterns += get_lots_urls()

    # Billing
    urlpatterns += get_billing_urls()

    # Docs
    urlpatterns += get_swagger_urls()

    return urlpatterns


urlpatterns = [
    path('api/v1/', include(build_server_urls())),
]
