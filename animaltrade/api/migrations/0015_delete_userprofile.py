# Generated by Django 3.1.5 on 2021-01-28 15:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0014_auto_20210127_2156'),
    ]

    operations = [
        migrations.DeleteModel(
            name='UserProfile',
        ),
    ]
