# Generated by Django 3.1.5 on 2021-01-28 15:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0016_auto_20210128_1516'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='wallet',
            index=models.Index(fields=['owner'], name='api_wallet_owner_i_a4633e_idx'),
        ),
        migrations.AddIndex(
            model_name='wallettransaction',
            index=models.Index(fields=['owner'], name='api_wallett_owner_i_393e08_idx'),
        ),
    ]
