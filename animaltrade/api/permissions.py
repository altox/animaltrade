from rest_framework import permissions
from rest_framework.request import Request

from animaltrade.api.models import Animal


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.method in permissions.SAFE_METHODS or obj.owner == request.user


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user


class IsOwnerOfAnimal(permissions.BasePermission):
    def has_permission(self, request: Request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        animal_pk = request.data.get('animal')

        if animal_pk is not None:
            animal = Animal.objects.filter(pk=animal_pk).first()
            owner = animal.owner if animal else None
            return owner == request.user
        else:
            return True