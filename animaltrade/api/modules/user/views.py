from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from animaltrade.api.modules.user.serializers import UserSerializer


class UserMeView(APIView):
    """
    Returns current user if authorized
    """

    def get(self, request):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=self.request.user.pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)
