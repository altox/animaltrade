from django.urls import path

from animaltrade.api.modules.user.views import UserMeView


def get_user_urls():
    return [
        path('users/me/', UserMeView.as_view())
    ]
