from django.test import TestCase
from rest_framework.authtoken.admin import User

from animaltrade.api.modules.billing.models import WalletTransaction
from animaltrade.api.modules.billing.wallet import WalletRepository


def get_test_user(name: str):
    existing = User.objects.filter(username=name)
    if existing:
        return existing

    return User.objects.create(username=name, password='test')


class WalletTests(TestCase):
    repo = WalletRepository()

    def test_get_wallet(self):
        user = get_test_user('wallet_test')
        wallet = self.repo.get_wallet(user.id)
        self.assertEqual(wallet.balance, 0)
        self.assertEqual(wallet.balance_locked, 0)

        balance = self.repo.get_available_balance(user.id)
        self.assertEqual(balance, 0)

    def test_deposit(self):
        user = get_test_user('wallet_test')
        self.repo.deposit(user.id, 100)
        balance = self.repo.get_available_balance(user.id)
        self.assertEqual(balance, 100)
        transactions = WalletTransaction.objects.filter(owner=user)[:10]
        self.assertEqual(transactions[0].amount, 100)
        self.assertEqual(transactions[0].type, 'deposit')
        self.assertEqual(transactions[0].balance_snapshot, 0)
        self.assertEqual(transactions[0].balance_locked_snapshot, 0)

    def test_transfer_should_fail_if_no_funds(self):
        user1 = get_test_user('wallet_test_1')
        user2 = get_test_user('wallet_test_2')

        with self.assertRaisesMessage(Exception, 'Insufficient funds'):
            self.repo.transfer(user1.id, user2.id, 100)

    def test_transfer(self):
        user3 = get_test_user('wallet_test_3')
        user4 = get_test_user('wallet_test_4')

        self.repo.deposit(user3.id, 100)

        self.repo.transfer(user3.id, user4.id, 10)

        self.assertEqual(self.repo.get_available_balance(user3.id), 90)
        self.assertEqual(self.repo.get_available_balance(user4.id), 10)

    def test_balance_lock(self):
        user5 = get_test_user('wallet_test_5')

        with self.assertRaisesMessage(Exception, 'Insufficient funds'):
            self.repo.lock_funds(user5.id, 10)

        with self.assertRaisesMessage(Exception, 'Internal inconsistency'):
            self.repo.release_funds(user5.id, 10)

        self.repo.deposit(user5.id, 100)
        self.repo.lock_funds(user5.id, 10)
        self.assertEqual(self.repo.get_available_balance(user5.id), 90)
        self.repo.release_funds(user5.id, 10)
        self.assertEqual(self.repo.get_available_balance(user5.id), 100)