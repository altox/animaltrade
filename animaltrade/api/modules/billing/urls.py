from django.urls import path

from animaltrade.api.modules.billing.views import WalletView, WalletDepositView, WalletTransactionsView


def get_billing_urls():
    return [
        path('wallet/', WalletView.as_view()),
        path('wallet/deposit', WalletDepositView.as_view()),
        path('wallet/transactions', WalletTransactionsView.as_view({'get': 'list'})),
    ]
