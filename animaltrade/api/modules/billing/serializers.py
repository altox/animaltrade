from rest_framework.authtoken.admin import User
from rest_framework.fields import IntegerField
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import Serializer, ModelSerializer

from animaltrade.api.modules.billing.models import WalletTransaction
from animaltrade.api.modules.user.serializers import UserSerializer


class WalletDepositSerializer(Serializer):
    user_id = PrimaryKeyRelatedField(many=False, write_only=False, queryset=User.objects.all())
    amount = IntegerField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class WalletTransactionSerializer(ModelSerializer):
    income_from = UserSerializer()
    outcome_to = UserSerializer()

    class Meta:
        model = WalletTransaction
        fields = ('id', 'amount', 'type', 'income_from', 'outcome_to', 'created')
