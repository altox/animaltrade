from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from animaltrade.api.modules.billing.models import WalletTransaction
from animaltrade.api.modules.billing.serializers import WalletDepositSerializer, WalletTransactionSerializer
from animaltrade.api.modules.modules import Modules


class WalletView(APIView):
    """
    Returns current user wallet if authorized
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        wallet = Modules.Billing.wallet.get_wallet(self.request.user.id)
        return Response({
            'balance': wallet.balance,
            'balance_locked': wallet.balance_locked
        })


class WalletDepositView(APIView):
    """
    Adds funds to specified user wallet
    """
    permission_classes = (IsAuthenticated, IsAdminUser)

    def post(self, request):
        serializer = WalletDepositSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        Modules.Billing.wallet.deposit(serializer.data.get('user_id'), serializer.data.get('amount'))
        return Response({'ok': True})

    def get_serializer(self):
        return WalletDepositSerializer()


class WalletTransactionsView(ListModelMixin, GenericViewSet):
    """
    Returns list of current user wallet transactions
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = WalletTransactionSerializer

    def get_queryset(self):
        return WalletTransaction.objects.filter(owner=self.request.user).order_by('created')
