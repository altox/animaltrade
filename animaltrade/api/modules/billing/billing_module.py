from animaltrade.api.modules.billing.wallet import WalletRepository


class BillingModule:
    def __init__(self, wallet: WalletRepository):
        self.wallet = wallet
