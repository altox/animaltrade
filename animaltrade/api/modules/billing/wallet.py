from django.db import transaction

from animaltrade.api.modules.billing.models import Wallet, WalletTransaction


class WalletRepository:

    @transaction.atomic
    def get_wallet(self, uid: int) -> Wallet:
        existing = Wallet.objects.filter(owner_id=uid).first()
        if existing is None:
            return Wallet.objects.create(owner_id=uid, balance=0, balance_locked=0)

        return existing

    @transaction.atomic
    def get_available_balance(self, uid: int) -> int:
        wallet = self.get_wallet(uid)
        return wallet.balance - wallet.balance_locked

    @transaction.atomic
    def deposit(self, uid: int, amount: int) -> Wallet:
        wallet = self.get_wallet(uid)

        # Create transaction
        WalletTransaction.objects.create(
            owner_id=uid,
            amount=amount,
            type='deposit',
            balance_snapshot=wallet.balance,
            balance_locked_snapshot=wallet.balance_locked
        )

        # Update wallet
        wallet.balance += amount
        wallet.save()

        return wallet

    @transaction.atomic
    def transfer(self, from_uid: int, to_uid: int, amount: int):

        wallet_from = self.get_wallet(from_uid)
        wallet_to = self.get_wallet(to_uid)

        from_wallet_balance = self.get_available_balance(from_uid)

        if from_wallet_balance < amount:
            raise Exception('Insufficient funds')

        # Create transactions
        WalletTransaction.objects.create(
            owner_id=from_uid,
            amount=amount,
            type='outcome',
            balance_snapshot=wallet_from.balance,
            balance_locked_snapshot=wallet_from.balance_locked,
            outcome_to_id=to_uid
        )

        WalletTransaction.objects.create(
            owner_id=to_uid,
            amount=amount,
            type='income',
            balance_snapshot=wallet_to.balance,
            balance_locked_snapshot=wallet_to.balance_locked,
            income_from_id=from_uid
        )

        # Update wallets
        wallet_from = self.get_wallet(from_uid)
        wallet_to = self.get_wallet(to_uid)

        wallet_from.balance -= amount
        wallet_to.balance += amount

        wallet_from.save()
        wallet_to.save()

    @transaction.atomic
    def lock_funds(self, uid: int, amount: int) -> Wallet:
        wallet = self.get_wallet(uid)
        if self.get_available_balance(uid) < amount:
            raise Exception('Insufficient funds')

        wallet.balance_locked += amount
        wallet.save()
        return wallet

    @transaction.atomic
    def release_funds(self, uid: int, amount: int) -> Wallet:
        wallet = self.get_wallet(uid)
        if wallet.balance_locked == 0 or wallet.balance_locked - amount < 0:
            raise Exception('Internal inconsistency')

        wallet.balance_locked -= amount
        wallet.save()
        return wallet

