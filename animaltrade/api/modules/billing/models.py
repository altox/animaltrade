from django.contrib.auth.models import User
from django.db.models import Model, ForeignKey, CASCADE, IntegerField, CharField, DateTimeField

"""
Money stored in cents
For security reasons models below should not be modified directly
"""


class Wallet(Model):
    owner = ForeignKey(User, related_name='wallet', on_delete=CASCADE, primary_key=True, db_index=True)
    balance = IntegerField()
    balance_locked = IntegerField()


TRANSACTION_TYPES = (
    ('deposit', 'deposit'),
    ('income', 'income'),
    ('outcome', 'outcome'),
)


class WalletTransaction(Model):
    created = DateTimeField(auto_now_add=True, db_index=True)
    owner = ForeignKey(User, related_name='wallet_transactions', on_delete=CASCADE, db_index=True)
    amount = IntegerField()
    type = CharField(choices=TRANSACTION_TYPES, max_length=100)
    balance_snapshot = IntegerField()
    balance_locked_snapshot = IntegerField()
    income_from = ForeignKey(User, related_name='wallet_in_transactions', on_delete=CASCADE, blank=True, null=True)
    outcome_to = ForeignKey(User, related_name='wallet_out_transactions', on_delete=CASCADE, blank=True, null=True)

