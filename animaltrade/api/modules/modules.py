from animaltrade.api.modules.billing.billing_module import BillingModule
from animaltrade.api.modules.billing.wallet import WalletRepository


class ModulesImpl:
    Billing = BillingModule(wallet=WalletRepository())


Modules = ModulesImpl()
