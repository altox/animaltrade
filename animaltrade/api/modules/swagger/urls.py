from django.conf.urls import url
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny

schema_view = get_schema_view(
    openapi.Info(
        title="AnimalTrade API",
        default_version='v1',
        description="Animal Trade API sandbox",
        contact=openapi.Contact(email="xeroxaltox@gmail.com"),
        license=openapi.License(name="WTFPL License"),
    ),
    public=True,
    permission_classes=[AllowAny],
)


def get_swagger_urls():
    return [
        url(r'^docs/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui')
    ]
