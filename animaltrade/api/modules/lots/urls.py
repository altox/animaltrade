from django.urls import path

from animaltrade.api.modules.lots.views import AnimalLotViewSet, MyAnimalLotsViewSet, LotBetsViewSet, AcceptBetView


def get_lots_urls():
    return [
        path('animal-lots/', AnimalLotViewSet.as_view({
            'get': 'list',
            'post': 'create'
        })),
        path('animal-lots/my/', MyAnimalLotsViewSet.as_view({
            'get': 'list',
        })),
        path('animal-lots/<int:pk>/', AnimalLotViewSet.as_view({
            'get': 'retrieve',
            'patch': 'partial_update',
            'delete': 'destroy'
        })),
        path('animal-lots/<int:pk>/bets/', LotBetsViewSet.as_view({
            'post': 'create',
            'get': 'list',
        })),
        path('animal-lots/<int:lot_pk>/bets/<int:bet_pk>/accept/', AcceptBetView.as_view()),
    ]
