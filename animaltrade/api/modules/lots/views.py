from django.db import transaction
from rest_framework.exceptions import APIException, NotFound
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from animaltrade.api.modules.lots.models import AnimalLot, LotBet
from animaltrade.api.modules.lots.serializers import AnimalLotSerializer, LotBetSerializer
from animaltrade.api.modules.modules import Modules
from animaltrade.api.permissions import IsOwnerOrReadOnly, IsOwnerOfAnimal


class AnimalLotViewSet(ModelViewSet):
    """
    Allows to get, update and delete lots
    """
    http_method_names = ['get', 'post', 'head', 'patch', 'delete']
    queryset = AnimalLot.objects.all()
    serializer_class = AnimalLotSerializer
    permission_classes = (
        IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
        IsOwnerOfAnimal
    )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def update(self, request, *args, **kwargs):
        lot = AnimalLot.objects.filter(pk=kwargs.get('pk')).first()
        if lot and lot.state is not 'open':
            raise APIException('Can\'t update lot with existing bets')

        return super(AnimalLotViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        lot: AnimalLot = self.get_object()
        if lot.state == 'in_progress':
            # Release last bet maker founds
            last_bet: LotBet = LotBet.objects.filter(lot=lot).order_by('-created').first()
            if last_bet:
                Modules.Billing.wallet.release_funds(last_bet.by.id, last_bet.amount)

        return super(AnimalLotViewSet, self).destroy(request, *args, **kwargs)


class MyAnimalLotsViewSet(ListModelMixin, GenericViewSet):
    """
    Returns list of current user lots
    """
    http_method_names = ['get']
    serializer_class = AnimalLotSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return AnimalLot.objects.filter(owner=self.request.user)


class LotBetsViewSet(CreateModelMixin, ListModelMixin, GenericViewSet):
    """
    Returns list of bets for specific lot & allows to create bet
    New bet amount should be greater then lot price & latest bet on lot
    """
    http_method_names = ['get', 'post']
    serializer_class = LotBetSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request, *args, **kwargs):
        if AnimalLot.objects.filter(pk=kwargs['pk']).first() is None:
            raise NotFound()

        return super(LotBetsViewSet, self).list(request, *args, **kwargs)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        lot_id = kwargs['pk']
        lot: AnimalLot = AnimalLot.objects.filter(pk=lot_id).first()
        amount = int(request.data.get('amount'))

        if lot is None:
            raise NotFound()

        if lot.state == 'closed':
            raise APIException(detail='Lot is closed already')

        if lot.owner == self.request.user:
            raise APIException(detail='Can\'t make bet on your own lot')

        if lot.price > amount:
            raise APIException(detail='New bet must be greater than lot price')

        last_bet: LotBet = LotBet.objects.filter(lot_id=lot_id).order_by('-created').first()

        if last_bet and last_bet.amount >= amount:
            raise APIException(detail='New bet must be greater than existing')

        uid = self.request.user.id

        balance = Modules.Billing.wallet.get_available_balance(uid)

        if balance < amount:
            raise APIException(detail='Insufficient funds')

        # Lock funds for current bet maker
        Modules.Billing.wallet.lock_funds(uid, amount)
        # Release funds for previous bet maker
        if last_bet:
            Modules.Billing.wallet.release_funds(last_bet.by.id, last_bet.amount)

        lot.state = 'in_progress'
        lot.save()

        return super(LotBetsViewSet, self).create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(by=self.request.user, lot_id=self.kwargs['pk'])

    def get_queryset(self):
        if self.kwargs.get('pk') is None:
            return None
        return LotBet.objects.filter(lot_id=self.kwargs['pk']).order_by('-created')


class AcceptBetView(APIView):
    """
    Accepts specific bet on lot
    If succeeds - lot is closed
    """
    permission_classes = (IsAuthenticated,)

    @transaction.atomic
    def post(self, request, lot_pk, bet_pk):
        lot: AnimalLot = AnimalLot.objects.filter(pk=lot_pk).first()
        bet: LotBet = LotBet.objects.filter(pk=bet_pk).first()

        if lot is None or bet is None:
            raise NotFound()

        if lot.owner != self.request.user:
            raise PermissionError()

        if lot.state == 'closed':
            raise APIException(detail='Lot is closed already')

        lot.state = 'closed'
        lot.winner_bet = bet
        lot.save()

        # Release funds
        Modules.Billing.wallet.release_funds(bet.by.id, bet.amount)
        # Transfer funds
        Modules.Billing.wallet.transfer(bet.by.id, lot.owner.id, bet.amount)
        # Move animal to new owner
        lot.animal.owner = lot.owner
        lot.save()

        return Response({'ok': True})
