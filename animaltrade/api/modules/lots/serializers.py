from rest_framework.fields import ReadOnlyField
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import HyperlinkedModelSerializer

from animaltrade.api.modules.animals.models import Animal
from animaltrade.api.modules.lots.models import AnimalLot, LotBet
from animaltrade.api.modules.user.serializers import UserSerializer


class LotBetSerializer(HyperlinkedModelSerializer):
    by = UserSerializer(read_only=True, many=False)

    class Meta:
        model = LotBet
        fields = (
            'id',
            'created',
            'amount',
            'by'
        )


class AnimalLotSerializer(HyperlinkedModelSerializer):
    animal = PrimaryKeyRelatedField(many=False, write_only=False, queryset=Animal.objects.all())
    state = ReadOnlyField(read_only=True)
    animal_name = ReadOnlyField(source='animal.name', read_only=True)
    animal_kind = ReadOnlyField(source='animal.kind', read_only=True)
    animal_type = ReadOnlyField(source='animal.type', read_only=True)
    owner = UserSerializer(read_only=True, many=False)
    winner_bet = LotBetSerializer(read_only=True, many=False)

    class Meta:
        model = AnimalLot
        fields = (
            'id',
            'created',
            'owner',
            'animal',
            'price',
            'state',
            'animal',
            'animal_name',
            'animal_kind',
            'animal_type',
            'winner_bet'
        )
