from django.db.models import Model, DateTimeField, ForeignKey, IntegerField, CharField, OneToOneField, CASCADE, PROTECT
from rest_framework.authtoken.admin import User

from animaltrade.api.modules.animals.models import Animal

ANIMAL_LOT_STATES = (
    ('open', 'open'),
    ('in_progress', 'in_progress'),
    ('closed', 'closed')
)


class AnimalLot(Model):
    created = DateTimeField(auto_now_add=True, db_index=True)
    owner = ForeignKey(User, related_name='lots', on_delete=CASCADE, db_index=True)
    animal = OneToOneField(Animal, related_name='lot', on_delete=CASCADE, db_index=True)
    price = IntegerField()  # stored in cents
    state = CharField(choices=ANIMAL_LOT_STATES, max_length=100, default='open')
    winner_bet = OneToOneField('LotBet', on_delete=PROTECT, blank=True, null=True)

    class Meta:
        ordering = ('created',)


class LotBet(Model):
    created = DateTimeField(auto_now_add=True, db_index=True)
    lot = ForeignKey(AnimalLot, related_name='bets', on_delete=CASCADE, db_index=True)
    by = ForeignKey(User, related_name='bets', on_delete=CASCADE, db_index=True)
    amount = IntegerField()  # stored in cents
