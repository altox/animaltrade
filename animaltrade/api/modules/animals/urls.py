from rest_framework.routers import SimpleRouter

from animaltrade.api.modules.animals.views import AnimalViewSet


def get_animals_urls():
    router = SimpleRouter()

    router.register(r'animals', AnimalViewSet)
    return router.urls