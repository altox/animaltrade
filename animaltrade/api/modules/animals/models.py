from django.contrib.auth.models import User
from django.db.models import Model, DateTimeField, ForeignKey, CharField, TextField, CASCADE

ANIMAL_TYPES = (
    ('cat', 'cat'),
    ('hedgehog', 'hedgehog')
)


class Animal(Model):
    created = DateTimeField(auto_now_add=True)
    owner = ForeignKey(User, related_name='owner', on_delete=CASCADE, db_index=True)
    type = CharField(choices=ANIMAL_TYPES, max_length=100)
    kind = TextField()
    name = TextField()

    class Meta:
        ordering = ('created',)
