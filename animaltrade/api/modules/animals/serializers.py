from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import HyperlinkedModelSerializer

from animaltrade.api.modules.animals.models import Animal


class AnimalSerializer(HyperlinkedModelSerializer):
    owner = ReadOnlyField(source='owner.username')

    class Meta:
        model = Animal
        fields = ('url', 'id', 'owner', 'type', 'kind', 'name', 'created')