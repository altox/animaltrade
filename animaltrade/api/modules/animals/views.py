from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from animaltrade.api.modules.animals.models import Animal
from animaltrade.api.modules.animals.serializers import AnimalSerializer
from animaltrade.api.permissions import IsOwner


class AnimalViewSet(ModelViewSet):
    """
    Allows to get, update and delete animals
    User can only get list of animals he own
    """
    http_method_names = ['get', 'post', 'head', 'patch', 'delete']
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer
    permission_classes = (
        IsAuthenticated,
        IsOwner
    )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(owner=self.request.user)
        return query_set