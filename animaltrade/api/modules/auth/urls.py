from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from animaltrade.api.modules.auth.views import RegisterView


def get_auth_urls():
    return [
        path('auth/signup/', RegisterView.as_view()),
        path('auth/token/', obtain_auth_token)
    ]
