from rest_framework import generics
from rest_framework.authtoken.admin import User
from rest_framework.permissions import AllowAny

from animaltrade.api.modules.auth.serializers import SignUpSerializer


class RegisterView(generics.CreateAPIView):
    """
    Registers new user
    """
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = SignUpSerializer
