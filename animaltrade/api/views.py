from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET'])
def ping_view(request):
    if request.method == 'GET':
        print(type(request.user.pk))
        return Response('pong', status=status.HTTP_200_OK)
